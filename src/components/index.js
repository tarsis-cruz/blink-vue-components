import Vue from 'vue'
import MainHeader from './MainHeader.vue'
import MainSidebar from './MainSidebar.vue'

const Components = {
  MainHeader,
  MainSidebar
};

export default {
  install(Vue, options) {
    Vue.component("main-sidebar", MainSidebar);
  }
};


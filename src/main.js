import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

const domain = 'http://viabilidade-blink.local:81';
const bildDomain = 'http://b-link.local:8084';

Vue.prototype.$domain = domain
Vue.prototype.$bildDomain = bildDomain

new Vue({
  render: h => h(App),
}).$mount('#app')

var loading = {
    start() {
        if (!$('.loader').length) {
            $('body').append('<div class="loader"></div>');
        }
    },
    stop() {
        if ($('.loader').length) {
            $('.loader').fadeToggle(function () {
                $(this).remove();
            });
        }
    }
};
export default loading;

import MainSidebar from './src/components/MainSidebar.vue';
import MainHeader from './src/components/MainHeader.vue';

function installMainSidebar(Vue) {
  if (installMainSidebar.installed) return
  installMainSidebar.installed = true
  Vue.component('MainSidebar', MainSidebar)
}

function installMainHeader(Vue) {
  if (installMainHeader.installed) return
  installMainHeader.installed = true
  Vue.component('MainHeader', MainHeader)
}

const plugin = {
  installMainSidebar,
  installMainHeader,
}

let GlobalVue = null
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue
} else if (typeof global !== 'undefined') {
  GlobalVue = global.vue
}

if (GlobalVue) {
  GlobalVue.use(plugin)
}

MainSidebar.install = installMainSidebar
MainHeader.install = installMainHeader

export const mainHeader = MainHeader;
export const mainSidebar = MainSidebar;

